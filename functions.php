<?php
/**
 * Renders the contents of the given template to a string and returns it.
 *
 * @param string $template_name The name of the template to render (without .php)
 * @param array  $attributes    The PHP variables for the template
 *
 * @return string               The contents of the template.
 */
function get_template_html( $template_name, $attributes = null ) {
    if ( ! $attributes ) {
        $attributes = array();
    }

    ob_start();

    do_action( 'personalize_login_before_' . $template_name );

    require( 'templates/' . $template_name . '.php');

    do_action( 'personalize_login_after_' . $template_name );

    $html = ob_get_contents();
    ob_end_clean();

    return $html;
}

?>
