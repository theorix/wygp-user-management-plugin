<div class="login-form-container">
    <!-- Show errors here if there are any -->
    <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
      <p class="login-username">
          <label for="user_login"><?php _e( 'User Name', 'wygp_user_management_plugin' ); ?></label>
          <input type="text" name="user_name" id="user_name">
      </p>
        <p class="login-username">
            <label for="user_email"><?php _e( 'Email', 'wygp_user_management_plugin' ); ?></label>
            <input type="email" name="user_email" id="user_email">
        </p>
        <p class="login-password">
            <label for="user_pass"><?php _e( 'Password', 'wygp_user_management_plugin' ); ?></label>
            <input type="password" name="pwd" id="user_pass">
        </p>
        <p class="login-username">
            <label for="gender"><?php _e( 'Gender', 'wygp_user_management_plugin' ); ?></label>
            <select  name="gender" id="gender">
              <option value="select">Select</option>
              <option value="male">Male</option>
              <option value="female">Female</option>
            </select>
        </p>
        <p class="login-submit">
            <input type="submit" value="<?php _e( 'Register', 'wygp_user_management_plugin' ); ?>">
        </p>
    </form>
</div>
