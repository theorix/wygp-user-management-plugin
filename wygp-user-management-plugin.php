<?php
/*
*  Plugin Name: WYGP user management
*  Author: Israel Duff
*  Author URL: http://localhost/theorix
*  Plugin URL: http://localhost/theorix
*  Description: This plugin is the user management plugin for the WYGP system
*  Version: 0.0.0
*/

/*
*               The user login module
* Creates the login pages at plugin activation
*/
require_once dirname(__FILE__)."/user-login.php";
register_activation_hook( __FILE__, array( 'User_Login', 'plugin_activated' ) );


/*
*               The user Registration module
* Creates the registration pages at plugin activation
*/
require_once dirname(__FILE__)."/user-registration.php";
register_activation_hook(__FILE__,array("User_Registration","plugin_activated"));


/*
* playground
*/
function portfolio_cpt() {
  $args = array('label' => 'Portfolio', 'public' => true);
  register_post_type("portfolio",$args);
}

add_action("init",'portfolio_cpt');







?>
