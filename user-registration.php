<?php
require_once "functions.php";

class User_Registration {
  /**
   * Initializes the plugin.
   *
   * To keep the initialization fast, only add filter and action
   * hooks in the constructor.
   */
   public function __construct() {  //filter and action hooks here
     add_shortcode("wygp-user-registration-form",array($this,"render_registration_form"));
   }


   /**
    *             Plugin activation hook
    * This function (or method) is executed when the plugin is activated.
    * It Creates all WordPress pages needed by the plugin.
    */
    public static function plugin_activated() {
      $registration_pages_definition = array(
        "member-registration" => array(
          "title" => "User Registration",
          "content" => "[wygp-user-registration-form]"
          )
      );


      foreach($registration_pages_definition as $slug => $page) {
        //check if the page doesn't already exist
        $query = new WP_Query("pagename=".$slug);
        if(!$query->have_posts()) { //there is no such page... create one
          wp_insert_post(array(
            "post_name" => $slug,
            "post_title" => $page['title'],
            "post_content" => $page['content'],
            "post_status" => 'publish',
            "post_type" => "page",
            "comment_status" => "closed",
            "ping_status" => "closed"
          )
        );
        }
      }
    }

    public function render_registration_form($attributes, $content = null) {

      /*
        first check wheither the form has been submited

      */
      if($_SERVER['REQUEST_METHOD'] == 'POST') {
        //get the user details
        $username = $_POST['user_name'];
        $user_email = $_POST['user_email'];
        $password = $_POST['pwd'];
        $gender = $_POST['gender'];

        //check if user name and email already exists
        $user_exists = username_exists($user_name) OR email_exists($user_email);
        if($check_user) exit;
        $done = wp_create_user($username,$password,$user_email);
      }

      //parse the short code attributes
      $default_attributes = array( 'show_title' => false );
      $attributes = shortcode_atts( $default_attributes, $attributes );

      // Pass the redirect parameter to the WordPress login functionality: by default,
      // don't specify a redirect, but if a valid redirect URL has been passed as
      // request parameter, use it.
      $attributes['redirect'] = '';
      if ( isset( $_REQUEST['redirect_to'] ) ) {
          $attributes['redirect'] = wp_validate_redirect( $_REQUEST['redirect_to'], $attributes['redirect'] );
      }

      return get_template_html("registration_form",$attributes);
    }
}

$user_registration = new User_Registration();

?>
